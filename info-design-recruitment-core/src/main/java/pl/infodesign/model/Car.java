package pl.infodesign.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDate dateOfPurchase;

    @Column
    private String color;

    protected Car() {
        // JPA
    }

    public Car(String[] data) {
        this.id = Long.valueOf(data[0]);
        this.name = data[1];
//        this.dateOfPurchase = LocalDate.parse(data[2]);
        this.color = data[3];
    }
}
