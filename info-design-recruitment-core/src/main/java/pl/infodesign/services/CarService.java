package pl.infodesign.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.infodesign.model.Car;
import pl.infodesign.repo.CarRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Service
public class CarService {

//    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    private String color = "Zielony";

    private String pathToCsv = "/home/piotr/Pulpit/pl2050-22grudnia/info-design-recruitment/info-design-recruitment-server/src/main/resources/samochody.csv";

    public void importCsv() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
        String row;
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",");
            for (int i = 0; i < data.length; i++) {
                saveCar(data);
            }
        }
        csvReader.close();
    }

    private void saveCar(String[] data) {
        if (data[3].equals(color)) {
            Car car = new Car(data);
            carRepository.save(car);
        }
    }

}
