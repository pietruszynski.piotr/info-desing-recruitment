package pl.infodesign.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.infodesign.model.Car;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

}
